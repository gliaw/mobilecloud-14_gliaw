/*
 * 
 * Copyright 2014 Jules White
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package org.magnum.mobilecloud.video;

import java.security.Principal;
import java.util.Collection;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.magnum.mobilecloud.video.repository.Video;
import org.magnum.mobilecloud.video.repository.VideoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Lists;

@Controller
public class MainController {
	
	@Autowired
	private VideoRepository videos;
	
	// TODO GET /video
	@RequestMapping(value = "/video", method = RequestMethod.GET)
	public @ResponseBody Collection<Video> getVideoList() {
		return Lists.newArrayList(videos.findAll());
	}
	
	// TODO GET /video/{id}
	@RequestMapping(value = "/video/{id}", method = RequestMethod.GET)
	public @ResponseBody Video getVideoById(@PathVariable("id") long id, HttpServletResponse response) {
		Video v = videos.findOne(id);
		
		// if video not found, return Service Code 404 Not Found
		if(v == null) {
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			return null;
		}
		
		return v;
	}
	
	// TODO POST /video
	@RequestMapping(value = "/video", method = RequestMethod.POST)
	public @ResponseBody Video addVideo(@RequestBody Video v) {
		return videos.save(v);
	}
	
	// TODO POST /video/{id}/like
	@RequestMapping(value = "/video/{id}/like", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<String> likeVideo(@PathVariable("id") long id, Principal p) {
		return updateLikes(id, p, true);
	}
	
	// TODO POST /video/{id}/unlike
	@RequestMapping(value = "/video/{id}/unlike", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<String> unlikeVideo(@PathVariable("id") long id, Principal p) {
		return updateLikes(id, p, false);
	}
	
	// TODO GET /video/search/findByName?title={title}
	@RequestMapping(value = "/video/search/findByName", method = RequestMethod.GET)
	public @ResponseBody Collection<Video> findByTitle(@RequestParam("title") String title) {
		return videos.findByName(title);
	}
	
	// TODO GET /video/search/findByDurationLessThan?duration={duration}
	@RequestMapping(value = "/video/search/findByDurationLessThan", method = RequestMethod.GET)
	public @ResponseBody Collection<Video> findByDurationLessThan(@RequestParam("duration") long duration) {
		return videos.findByDurationLessThan(duration);
	}
	
	// TODO GET /video/{id}/likedby
	@RequestMapping(value = "/video/{id}/likedby", method = RequestMethod.GET)
	public @ResponseBody Collection<String> getUsersWhoLikedVideo(@PathVariable("id") long id, HttpServletResponse response) {
		Video v = videos.findOne(id);
		
		// if video not found, return Service Code 404 Not Found
		if(v == null) {
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			return null;
		}
		
		return v.getLikedBy();
	}
	
	@RequestMapping(value="/go",method=RequestMethod.GET)
	public @ResponseBody String goodLuck(){
		return "Good Luck!";
	}
	
	// helper method for like/unlike methods
	private ResponseEntity<String> updateLikes(long id, Principal p, boolean like) {
		String username = p.getName();
		Video v = videos.findOne(id);
		Set<String> likedBy;
		
		// if video not found, return Service Code 404 Not Found
		if (v == null) {
			return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
		}
		
		likedBy = v.getLikedBy();
		
		if (like) {
			// if user already liked the video, return Service Code 400 Bad Request
			if (!likedBy.add(username)) {
				return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
			}
			// else update likedBy list, increment likes, return Service Code 200 OK
			else {
				v.setLikes(v.getLikes() + 1);
			}
		} else {
			// if user had not already liked the video, return Service Code 400 Bad Request
			if (!likedBy.remove(username)) {
				return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
			}
			// else update likedBy list, decrement likes, return Service Code 200 OK
			else {
				v.setLikes(v.getLikes() - 1);
			}
		}
		
		v.setLikedBy(likedBy);
		videos.save(v);
		return new ResponseEntity<String>(HttpStatus.OK);
	}
	
}
